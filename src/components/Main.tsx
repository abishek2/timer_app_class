import React from "react";
import "./Main.css"

interface Props {

}
interface State {
  timerVal: number[],
}
export class Main extends React.Component<Props, State>{
  timerCount: number;
  constructor() {
    super({});
    this.state = { timerVal: [0, 0, 0, 0] };
    this.timerCount = 86407;
  }

  componentDidMount() {
    setInterval(() => { this.timerCount--; this.setTime(); }, 1000);
  }

  setTime() {
    let tempTimerVal = this.timerCount;
    let newTime: number[] = [0, 0, 0, 0];
    if(tempTimerVal<=0){
      tempTimerVal=86407;
    }
    if (tempTimerVal / 86400 >= 1) {
      newTime[0] = Math.floor(tempTimerVal / 86400);
      tempTimerVal %= 86400;
    }
    if (tempTimerVal / 3600 >= 1) {
      newTime[1] = Math.floor(tempTimerVal / 3600);
      tempTimerVal %= 3600;
    }
    if (tempTimerVal / 60 >= 1) {
      newTime[2] = Math.floor(tempTimerVal / 60);
      tempTimerVal %= 60;
    }
    newTime[3] = tempTimerVal;
    this.setState({ timerVal: newTime })
  }

  clearTimer(){
    this.timerCount=86407;
  }

  render() {
    return (<main>
      <section className="headerSection">
        <div className="title">Countdown Timer</div>
        <div className="buttonsSection">
          <button onClick={this.clearTimer.bind(this)} className="button clearButton">Clear</button>
          <button className="button settingsButton">Settings</button>
        </div>
      </section>

      <section className="contentSection">
        <div className="mainContent">
          <div className="titleContent">
            Countdown ends in...
          </div>

          <div className="timeSection">

            <div className={`time time${this.state.timerVal[0]}`}>
              <div>{this.state.timerVal[0]}</div>
              <div className="label">Days</div>
            </div>

            <div className={`time time${this.state.timerVal[1]}`}>
              <div>{this.state.timerVal[1]}</div>
              <div className="label">Hours</div>
            </div>

            <div className={`time time${this.state.timerVal[2]}`}>
              <div>{this.state.timerVal[2]}</div>
              <div className="label">Mins</div>
            </div>

            <div className={`time time${this.state.timerVal[3]}`}>
            <div>{this.state.timerVal[3]}</div>
              <div className="label">Secs</div>
            </div>

          </div>
        </div>
      </section>

    </main>);
  }
}